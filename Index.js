var express = require('express');
var chalk = require('chalk');
var app = express();
var router = express.Router();
var fs = require('fs');
var parser = require('body-parser');
const mongodb = require('mongodb');

app.use(parser.json()); //support for json encoded bodies
app.use(parser.urlencoded({extend: true})); //support for url encoded bodies

let uri = 'mongodb://heroku_xjkrmb1w:tqofapkosn9j7mtmvuark4j4g@ds247569.mlab.com:47569/heroku_xjkrmb1w';

mongodb.MongoClient.connect(uri, function(err, client) {

    if(err) throw err;
  
    /*
     * Get the database from the client. Nothing is required to create a
     * new database, it is created automatically when we insert.
     */
  
    let db = client.db('heroku_xjkrmb1w')
  
    let Emrs = db.collection('EMR');

    var Error = "Error: The Specified Record Is Currently In Use";

    router.get('/emr', function(req, res) {
        /*
         *Here we utilize the mongoDb find() method to gather all our results residing in our database.
         *Since it would be impractical to lock all of the records every time they are called in mass;
         *We leave this Get without a lock
         */

        Emrs.find().toArray(function (err, docs) {
            res.json(docs);
        });
    });

    //Uses the id parameter in the url to find the matching record
    router.get('/emr/:id', (req, res) => {
        //We are going to first declare the name of the mongoDb default Id variable which is '_id' 
        var name = '_id';
        //Next we store the number that is being requested for a match & declare an empty query
        var value = parseInt(req.params.id);
        var query = {};
        //Here we build the query by assigning the mongo default '_id' name as the key and our value variable as the value
        //This creates a query that can be interpreted by the mongoDb findOne() method
        query[name] = value;
        
        //We are functionalizing an unlock so that we may delay it.
        function finish(item){
            item.lock = false;
            Emrs.findOneAndUpdate(query, item);
        }

        Emrs.findOne(query, function (err, item) { 
            //If the item is locked, let us know
            if(item.lock == true){
                res.send(Error);
            }
            else{
                //If the item is not locked, then update the record with a lock
                item.lock = true;
                Emrs.findOneAndUpdate(query, item);

                /* 
                 * Now, we wait 1.5 seconds before unlocking the item
                 * If you do not place a delay,
                 * it will be almost impossible to see that the lock is working
                 */ 
                setTimeout(finish, 1500, item);
                res.json(item);
            }
        });
    });

    router.post('/emr', (req, res) => {
        //urlencoded is needed for the post method if using Post Man

        /* 
         * First we store the request body.
         * We are assigning the body a lock variable
         * but it shall initially be false
         */
        var body = req.body;
        body.lock = false;
    
        var id = 0;

        /*
         * We are now utilizing the find() method again.
         * This time we convert the output to an array variable 'docs' to find the length of our database
         * Now we can give our body the next available slot in the database
        */
        Emrs.find().toArray(function (err, docs) {
            id = docs.length;
            body._id = id;
            Emrs.insertOne(body);
        });
    
        res.status(201).json(body);
    
    });

    router.delete('/emr/:id', (req, res) => {
        var name = '_id';
        var value = parseInt(req.params.id);
        var query = {};
        query[name] = value;

        //We made our query, now lets check if the record is locked

        Emrs.findOne(query, function (err, item) {
            if(item.lock == true){
                res.send(Error);
            }
            else{
                Emrs.findOneAndDelete(query);
                res.status(204).json(query);
            }
        });
    });

    //Updating
    router.put('/emr/:id', (req, res) => {
        var body = req.body;

        var name = '_id';
        var value = parseInt(req.params.id);
        var query = {};
        query[name] = value;

        Emrs.findOne(query, function (err, item) {

            /* 
             * Just as before we check for our lock
             * If it is not locked:
             * We lock it, update it, then unlock it after 1.5 seconds
             */

            if(item.lock == true){
                res.send(Error);
            }
            else{
                body.lock = true;
                Emrs.findOneAndUpdate(query, body);
                res.status(201).json(body);
                setTimeout(finish, 1500, body);
            }

        });

        function finish(body){
            body.lock = false;
            Emrs.findOneAndUpdate(query, body);
        }

    });

});


app.use('/rest', router);

app.listen(process.env.PORT || 5000, function(err) {

    if (err) {

        console.log(chalk.red(err));

    } else {

        console.log(chalk.blue('Magic Happens on Port 69'));

    }

});